const router = require('express').Router();
const {
	newPlace,
	readAllPlaces,
	readPlaceById,
	updatePlace,
	deleteYourPlace,
} = require('../controllers//placeToBooking.controllers');

router.get('/readAll', readAllPlaces);
router.get('/readById/:id', readPlaceById);
router.post('/create', newPlace);
router.put('/update/:id', updatePlace);
router.delete('/delete/:id', deleteYourPlace);

module.exports = router;
