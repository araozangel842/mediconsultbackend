const router = require('express').Router();

const {
	checkAllRooms,
	checkRoomsByCity,
	getRoomById,
	addRoom,
	updateRoom,
	removeRoom,
} = require('../controllers/adminNewRoom.controller');

router.get('/adminReadRooms', checkAllRooms);
router.get('/adminReadRoomsByCity', checkRoomsByCity);
router.get('/adminGetRoomById/:id', getRoomById);
router.post('/adminAddRoom', addRoom);
router.put('/adminUpdateRoom/:id', updateRoom);
router.delete('/adminDeleteRoom/:id', removeRoom);

module.exports = router;
