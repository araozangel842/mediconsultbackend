const router = require('express').Router();

const { LoginUser, NewUser, CheckIfUserExists } = require('../controllers/newUser.controller');

router.post('/login', LoginUser);
router.post('/create', NewUser);
router.post('/check', CheckIfUserExists);

module.exports = router;
