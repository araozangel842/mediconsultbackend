const controllerArray = {};
const PlaceToBooking = require('../models/placeToBooking.model');
const mongoose = require('mongoose');

controllerArray.readAllPlaces = async (req, res) => {
	const allPlaces = await PlaceToBooking.find();
	try {
		if (allPlaces.length) {
			res.status(200).send(allPlaces);
		} else {
			res.status(404).send('Not places to read');
		}
	} catch (error) {
		res.status(400).send(error.message);
	}
};
controllerArray.readPlaceById = async (req, res) => {
	const { id: _id } = req.params;

	if (!mongoose.Types.ObjectId.isValid(_id))
		return res.status(404).send('Room ID is invalid or not found');
	const placeById = await PlaceToBooking.findById({ _id: _id });
	try {
		res.status(200).send(placeById);
	} catch (error) {
		res.status(404).send('Room ID is invalid or not found');
	}
};
controllerArray.newPlace = async (req, res) => {
	const place = req.body;

	const newBooking = new PlaceToBooking(place);
	try {
		await newBooking.save();
		res.status(200).send({ message: 'Booking saved successfully' });
	} catch (error) {
		res.status(400).send({ error: error, message: 'Error saving booking' });
	}
};

controllerArray.updatePlace = async (req, res) => {
	const { id: _id } = req.params;
	const place = req.body;

	if (!mongoose.Types.ObjectId.isValid(_id))
		return res.status(404).send('Room ID is invalid or not found');

	try {
		await PlaceToBooking.findByIdAndUpdate(_id, place);
		res.status(200).send({ message: 'Room updated successfully' });
	} catch (error) {
		res.status(400).send({ error: error, message: 'Something went wrong updating' });
	}
};

controllerArray.deleteYourPlace = async (req, res) => {
	const { id: _id } = req.params;
	if (!mongoose.Types.ObjectId.isValid(_id))
		return res.status(404).send('Room ID is invalid or not found');
	try {
		await PlaceToBooking.findByIdAndRemove(_id);
		res.json({ message: 'Room deleted successfully' });
	} catch (error) {
		res.status(400).send({ error: error, message: 'Something went wrong deleting' });
	}
};

module.exports = controllerArray;
