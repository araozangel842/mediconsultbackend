const controllerArray = {};
const NewUserModel = require('../models/newUser.model');
const jwt = require('jsonwebtoken');

const { authenticate } = require('../middleware/lapdjs.middleware');

controllerArray.CheckIfUserExists = async (req, res) => {
	const { email } = req.body;
	console.log(req.body);

	const userExists = await NewUserModel.find({ userMail: email });

	if (!userExists) {
		res.status(404).send({ message: 'User not found' });
	} else {
		res.status(200).json({ message: 'User found', user: userExists });
	}
};

controllerArray.LoginUser = async (req, res) => {
	//the body has to be created from the login from Microsoft API
	//i have to grab the info and send it to the server
	// res.send('User sign in');
	const { email } = req.body;
	const userTokenSession = await jwt.sign(
		{
			email,
		},
		process.env.PRIVATE_KEY,
		{
			expiresIn: '1h',
		}
	);
	res.status(200).json({ message: 'User signed in successfully', userToken: userTokenSession });
};

controllerArray.NewUser = async (req, res) => {
	// res.send('User created');
	const {
		displayName,
		userStatus,
		userLoginId,
		userMail,
		mobilePhone,
		officeLocation,
		defaultSearch,
	} = req.body;
	try {
		const newUser = new NewUserModel({
			displayName,
			userStatus,
			userLoginId,
			userMail,
			mobilePhone,
			officeLocation,
			defaultSearch,
		});

		//Save the new user into the database
		const userSignUp = await newUser.save();

		//If success, send the user
		res.status(200).send(userSignUp);
	} catch (error) {
		console.log(error);
	}
};

module.exports = controllerArray;
