const controllerArray = {};
const AdminModelOfRooms = require('../models/adminNewRoom.model');
const mongoose = require('mongoose');

controllerArray.checkAllRooms = async (req, res) => {
	const allRooms = await AdminModelOfRooms.find();
	try {
		if (allRooms.length) {
			res.status(200).send(allRooms);
		} else {
			res.status(404).send('Room status not found');
		}
	} catch (error) {
		console.log(error);
	}
};

controllerArray.checkRoomsByCity = async (req, res) => {
	const oneRoom = await AdminModelOfRooms.find({ city: 'Turku' });
	console.log(oneRoom);
	res.status(200).send(oneRoom);
};

controllerArray.getRoomById = async (req, res) => {
	const { id: _id } = req.params;

	try {
		const room = await AdminModelOfRooms.findById({ _id: _id });
		res.status(200).send(room);
	} catch (error) {
		console.log(error);
	}
};

controllerArray.addRoom = async (req, res) => {
	const room = req.body;
	console.log(room.room);
	//Check if room already exists, if true return
	const existRoomNumber = AdminModelOfRooms.find({ room: room.room });
	// console.log(existRoomNumber);
	// if (existRoomNumber) return res.status(400).send('Room already exists');
	//
	if (existRoomNumber) console.log('Room already exists ' + existRoomNumber);
	//if not, continue with next request
	const newRoom = new AdminModelOfRooms(room);
	try {
		await newRoom.save();
		res.status(200).json(newRoom);
	} catch (error) {
		res.status(404).json({ error: error, message: 'Something went wrong' });
	}
};

controllerArray.updateRoom = async (req, res) => {
	const { id: _id } = req.params;
	const body = req.body;
	console.log(body);

	if (!mongoose.Types.ObjectId.isValid(_id))
		return res.status(404).send('Room ID is invalid or not found');
	try {
		await AdminModelOfRooms.findByIdAndUpdate(_id, body);
		res.status(200).json({ message: 'Room updated successfully' });
	} catch (error) {}
};

controllerArray.removeRoom = async (req, res) => {
	const { id: _id } = req.params;

	if (!mongoose.Types.ObjectId.isValid(_id))
		return res.status(404).send('Room ID is invalid or not found');

	try {
		await AdminModelOfRooms.findByIdAndRemove(_id);
		res.json({ message: 'Room removed successfully from database' });
	} catch (error) {
		res.status(400).send({ error: error.message, message: 'Something went wrong' });
	}
};

module.exports = controllerArray;
