const helper = {};

helper.time = () => {
	const time = new Date().getTime();
	return time;
};

module.exports = helper;
