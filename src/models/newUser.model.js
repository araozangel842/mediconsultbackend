const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
	displayName: String,
	userStatus: { type: String, default: 'user' },
	userLoginId: String,
	userMail: String,
	mobilePhone: String,
	officeLocation: String,
	defaultSearch: { type: String, default: 'Turku' },
	createdAt: {
		type: Date,
		default: new Date(),
	},
});

module.exports = mongoose.model('NewUserModel', UserSchema, 'usersDataToUse');
