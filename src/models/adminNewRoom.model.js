const mongoose = require('mongoose');

const AdminSchema = mongoose.Schema({
	adminId: String,
	city: String,
	room: { type: String, unique: true },
	parking: Boolean,
	chairs: [
		{
			userName: String,
			userEmail: String,
			isActive: { type: Boolean, default: true },
			otherScreens: { type: Boolean, default: true },
			chair: { type: Boolean, default: true },
			camera: { type: Boolean, default: true },
			laptop: { type: Boolean, default: true },
			keyboard: { type: Boolean, default: true },
			notes: String,
		},
	],
	createdAt: {
		type: Date,
		default: new Date(),
	},
});

module.exports = mongoose.model('AdminModelOfRooms', AdminSchema, 'Turku');
