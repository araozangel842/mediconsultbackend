const mongoose = require('mongoose');

const PlaceToBookingSchema = mongoose.Schema({
	userId: String,
	city: String,
	rooms: String,
	parking: { type: Boolean, default: false },
	//parking: Boolean,
	//booking_Date: { type: Date, index: true, default: Date.now() },
	booking_Date: String,
	chairType: String,
	created_At: {
		type: Date,
		index: true,
		default: Date.now(),
	},
});

module.exports = mongoose.model('PlaceToBooking', PlaceToBookingSchema, 'Reservations');
