const mongoose = require('mongoose');
const { database } = require('../keytoserver/key');

mongoose
	.connect(database.URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then((db) => console.log('Connected to ' + db.connection.name + ' in MongoDB'))
	.catch((err) => console.error(err));
