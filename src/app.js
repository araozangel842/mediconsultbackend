const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const { serverPORT } = require('./keytoserver/key');

//All routes here
const adminRoutes = require('./routes/adminNewRoom.routes');
const usersRoutes = require('./routes/newUser.routes');
const bookingRoutes = require('./routes/placeToBooking.routes');

module.exports = (app) => {
	//Port settings
	app.set('port', serverPORT.PORT || '');

	//Cors settings
	let corsOptions = {
		origin: 'http://localhost:3000',
		credentials: true,
		optionSuccessStatus: 200,
	};
	app.use(cors(corsOptions));
	app.use(morgan('dev'));

	//Middleware settings
	app.use(express.json());
	app.use(express.urlencoded({ extended: false }));

	//All routers
	app.use('/admin', adminRoutes);
	app.use('/users', usersRoutes);
	app.use('/booking', bookingRoutes);

	//Backend running server
	app.get('/', (req, res) => {
		res.send('The server is running');
	});

	return app;
};
