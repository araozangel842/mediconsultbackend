const authLDAP = {};
const ldap = require('ldapjs');

authLDAP.authenticate = (user, password) => {
	// const client = ldap.createClient({
	// 	url: ['ldap://127.0.0.1:3001', 'ldap://127.0.0.2:3001'],
	// 	//,
	// });
	// client.on('error', (err) => {
	// 	// handle connection error
	// 	console.log('Error on ' + err);
	// });
	// client.bind(user, password, (err, result) => {
	// 	if (err) {
	// 		console.log('Error in new client: ' + err);
	// 		return;
	// 	}
	// 	console.log('all done');
	// 	console.log('result: ' + result);
	// });
	const server = ldap.createServer();

	server.listen(389, '172.0.0.1', (err, result) => {
		if (err) {
			console.log(err);
			return;
		}
		console.log(console.log('LDAP server listening on port ' + server.url));
		console.log(result);
	});
};

module.exports = authLDAP;
